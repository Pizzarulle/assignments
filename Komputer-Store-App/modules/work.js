const Work = () => {
    let paycheck = 0
    /**
     *  Returns the value of {@link paycheck}
     * @returns {int} Value of paycheck
     */
    const getPaycheck = () => paycheck

    /**
     * Increases the value of {@link paycheck} by 100
     */
    const goWork = () => {
        paycheck += 100
    }

    /**
     *  Sets the value of {@link paycheck} to zero
     */
    const resetPaycheck = () => {
        paycheck = 0
    }

    return {
        getPaycheck,
        goWork,
        resetPaycheck
    }
}
export default Work