const paycheckElement = document.getElementById("span-paycheck")
const bankBalanceElement = document.getElementById("span-savings")

const loanBalanceElement = document.getElementById("span-loanAmount")
const loanParagraphElement = document.getElementById("p-loan")
const repayLoanBtn = document.getElementById("btn-repayLoan")

/**
 * Updates the innerText of {@link paycheckElement}
 * @param {int} value New value
 */
const updatePaycheckElements = (value) => {
    paycheckElement.innerText = new Intl.NumberFormat('sv-SV', { style: 'currency', currency: 'SEK' }).format(value)
}

/**
 * Updates the innerText of {@link bankBalanceElement}
 * @param {int} value New value 
 */
const updateBankElements = (value) => {
    bankBalanceElement.innerText = new Intl.NumberFormat('sv-SV', { style: 'currency', currency: 'SEK' }).format(value)
}

/**
 *  Updates the innerText of {@link loanBalance} and shows/hides {@link loanParagraphElement} and {@link repayLoanBtn} 
 * @param {int} value New value 
 */
const updateLoanElements = (value) => {
    loanBalanceElement.innerText = new Intl.NumberFormat('sv-SV', { style: 'currency', currency: 'SEK' }).format(value)
    loanParagraphElement.style.visibility = (value === 0) ? "hidden" : "visible"
    repayLoanBtn.style.visibility = (value === 0) ? "hidden" : "visible"
}

/**
 * Uses functions to update the innerText of both {@link bankBalanceElement} {@link loanBalanceElement}
 * 
 * @param {int} savingsValue New value
 * @param {int} loanValue New value
 */
const updateSavingsAndLoanElements = (savingsValue, loanValue) => {
    updateBankElements(savingsValue)
    updateLoanElements(loanValue)
}

export { updatePaycheckElements, updateSavingsAndLoanElements }